$(function(){
	$(document)
		.on('mouseenter', '[data-hover-id]', function(){
			$('[data-backid="' + $(this).data('hover-id') + '"]')
				.addClass('is-show')
				.siblings()
				.removeClass('is-show');
		})
		.on('mouseleave', '[data-hover-id]', function(){
			$('[data-backid]').removeClass('is-show');
		});

	var historySwiper = new Swiper('.js-historyCarouselSwiper .swiper-container', {
	    speed: 500,
	    slidesPerView: 3,
	    nextButton: '.js-historyCarouselSwiper .carousel-button.next',
	    prevButton: '.js-historyCarouselSwiper .carousel-button.prev',
	    breakpoints: {
	    	6000: {
	    		slidesPerView: 3
	    	},
	    	767: {
	    		slidesPerView: 1,
	    		autoHeight: true
	    	}
	    }
	});

	$(window).on('load resize', function(){
		if($('.js-asideTextPosition').length > 0){
			$('.js-asideTextPosition').css({
				'top': $('.method__list').offset().top
			});
		}

		if($(window).width() >= 768){
			$('.methods-screen__first').css({
				'height': $(window).height() - 130
			});

			if($('.methods__modal').length > 0){
				$('.methods__modal').css({
					'top': $('.method__list').offset().top - 155
				});
			}
		}
	});

	$(document)
		.on('click', '.js-methodModalLink', function(event){
			event.preventDefault();
			$('body').addClass('is-method-modal-open');
		})
		.on('click', '.js-methodModalClose', function(){
			$('body').removeClass('is-method-modal-open');
		});
});