$(function(){
	var $specialistGrid = $('.js-specialistGrid').isotope({
		itemSelector: '.specialist-col',
		layoutMode: 'fitRows'
	});

	$(document).on('click', '.js-specialistNav a', function(){
		$(this).parent().addClass('is-active').siblings().removeClass('is-active');
		specialistFilter($(this).data('filter'));
	});

	$(document).on('change', 'select.js-specialistSelect', function(){
		specialistFilter($(this).val());
	});

	function specialistFilter(val){
		var selected = val;
		
		$specialistGrid.isotope({
			filter: selected
		});
	}

});