$('[data-fancybox]').fancybox({
    clickOutside: 'close',
    toolbar : true,
    buttons : [
        'close'
    ],
    mobile: {
        clickSlide : function( current, event ) {
            return current.type === 'image' ? 'close' : false;
        }
    }
});