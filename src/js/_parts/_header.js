$(function(){
	var $body = $('body'),
		$menu = $('.menu');

	$(document)
		.on('click', '.js-burgerMenu', function(){
			var _this = $(this);

			if(_this.hasClass('is-active')){
				_this.removeClass('is-active');
				$menu.hide();
				$body.removeClass('is-menu-open is-form-open');

				if($(window).width() <= 767){
					$('.header--inner .logo__icon').removeAttr('style');
				}
			}
			else {
				_this.addClass('is-active');
				$menu.show();
				setTimeout(function(){
					$body.addClass('is-menu-open');

					if($(window).width() <= 767 && $('.header--inner').length > 0){
						$(document).trigger('logoAnimateTrigger');
					}
				});
			}
		})
		.on('mouseenter', '.contact-control', function(){
			if($(window).width() >= 1201){
				$(this).width($(this).find('.contact-control__label').outerWidth());
			}
		})
		.on('mouseleave', '.contact-control', function(){
			if($(window).width() >= 1201){
				$(this).width('');
			}
		})
		.on('click', '.js-menuRequestLink', function(){
			$body.addClass('is-form-open');
		})
		.on('click', '.js-menuRequestClose', function(){
			$body.removeClass('is-form-open');
		});
});