/*
 * Карта
 */

function initializeMap(){
	var WINDOW_WIDTH = $(window).width(),
		map = $('.js-map'),
		pin = map.data('pin');

	map.each(function(){
		var map, 
			loc = $(this).data('location').split(','),
			desktopLoc = WINDOW_WIDTH > 767 ? +loc[1] + 0.09 : +loc[1] + 0.02,
			position = new google.maps.LatLng(loc[0], loc[1]),
			mapOptions = {
		        center: new google.maps.LatLng(loc[0], loc[1]),
		        zoom: 17,
		        mapTypeId: google.maps.MapTypeId.ROADMAP,
		        mapTypeControlOptions: {
		            
		        },
		        disableDefaultUI: true,
		        scrollwheel: false,
		        rotateControl: false,
		        // styles: [{featureType:"all", elementType:"all", stylers:[{saturation:-100}, {gamma:1}]}]
	    	};

	    map = new google.maps.Map($(this).get(0), mapOptions);
	    map.setTilt(45);

	    var marker = new google.maps.Marker({
	        position: position,
	        map: map,
	        title: '',
	        animation: google.maps.Animation.DROP,
	        icon: {
	            url: pin,
	            scaledSize: new google.maps.Size(70, 87)
	        }
	    });

		map.setOptions({draggable: WINDOW_WIDTH > 767});
	});
}

if($('body').find('.js-map').length){
	google.maps.event.addDomListener(window, 'load', initializeMap);
}

$(function(){
	var $body = $('body');

	$(document).on('click', '.js-contactRequestLink', function(){
		$body.addClass('is-request-open');
	});

	$(document).on('click', '.js-contactRequestClose', function(){
		$body.removeClass('is-request-open');
	});
});