$(function(){

	var aboutSwiper = new Swiper('.js-aboutPhotosSwiper .swiper-container', {
	    speed: 500,
	    loop: true,
	    slidesPerView: 1,
	    nextButton: '.js-aboutPhotosSwiper .carousel-button.next',
	    prevButton: '.js-aboutPhotosSwiper .carousel-button.prev',
	    pagination: '.js-aboutPhotosSwiper .swiper-pagination',
	    onSlideChangeStart: function(swiper){
	    	var active = swiper.realIndex + 1;

	    	$('.swiper-count__active').text(active);
	    }
	});

	$(window).on('load resize', function(){
		if($('.about__screen').length > 0){
			aboutHeight();
		}
	});

	function aboutHeight(){
		if($(window).width() <= 767){
			$('.about__screen').css({
				'min-height': $(window).height() - $('.about__screen').offset().top 
			});
		}
	}

	if($('.about__screen').length > 0){
		aboutHeight();
	}
});