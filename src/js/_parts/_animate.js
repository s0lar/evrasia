$(function(){
	var logoShift = 0;
	var isMobile = false;

	function animateLogo() {
		logoShift = 0;
		isMobile = window.innerWidth < 768;
		setTimeout(function () {
			logoAnimStep();
		}, 500)
	}

	function logoAnimStep() {
		var logo = $('.logo__icon');

		logo.css({'opacity': '1'});

		setTimeout(function () {
			logoShift += isMobile ? 27 : 35; // width
			if (logoShift >= (isMobile ? 756 : 980)) {
				return;
			}
			logo.css('background-position', '-' + logoShift + 'px 0');
			logoAnimStep();
		}, 40);
	}

	$(window).on('load', function(){
		if($('body').hasClass('animate')){
			setTimeout(function(){
				animateLogo();
			}, 800);
		}
		else {
			animateLogo();	
		}

		$('.animate').removeClass('animate');
	});

	$(document).on('logoAnimateTrigger', function(){
		animateLogo();
	});
});